---
id: d91ec932-1f29-402c-bb54-85c7e8fdb422
blueprint: contacts
title: 'Cristina Fresneda'
first_name: Cristina
last_name: Fresneda
job_title: 'Frontend Developer'
partner:
  - Partner
client:
  - Client
updated_by: 1
updated_at: 1638623347
seo_description: Cristina
seo_date: '2021-12-04'
seo_open_graph_image: contacts/doraemon-2.jpg
thumbnail: contacts/bl-10.jpg
---
