---
id: 2187435c-e949-455e-a764-31c82e818f53
blueprint: contacts
title: 'Alvaro Rodríguez'
first_name: Álvaro
last_name: Rodríguez
job_title: 'Backend Developer'
thumbnail: contacts/doraemon-1.jpg
seo_description: 'Lorem ipsum'
seo_date: '2021-12-03'
seo_open_graph_image: contacts/doraemon-1.jpg
updated_by: 1
updated_at: 1638556404
---
