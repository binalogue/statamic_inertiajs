---
id: 6f6f5914-606b-4d9b-99dd-e535c664b9d5
blueprint: page_home
title: Homepage
featured_a:
  - 43a5737d-236e-4966-9944-52eb1a9b347c
content_bard:
  -
    type: set
    attrs:
      values:
        type: title
        title: 'Bard Title HomePage'
  -
    type: set
    attrs:
      values:
        type: text
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, '
        width: 100%
        position: Left
  -
    type: paragraph
updated_by: 2
updated_at: 1639134129
featured_b:
  - a2769953-a952-4d42-a0c2-ea374b59b10f
  - f2b5a763-ba33-4aed-9856-984a3c840566
seo_description: 'HomePage Seo Description'
seo_date: '2021-12-09'
seo_open_graph_image: contacts/bl-13.jpg
---
