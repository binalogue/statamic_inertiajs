---
id: 87ed4af1-2276-4a27-94a0-9bdee0d31ed2
blueprint: page_about
title: About
content_bard:
  -
    type: set
    attrs:
      values:
        type: title
        title: About
  -
    type: paragraph
seo_description: 'Lorem ipsum'
seo_date: '2021-12-04'
seo_open_graph_image: contacts/bl-16.jpg
partners:
  - 2187435c-e949-455e-a764-31c82e818f53
  - d91ec932-1f29-402c-bb54-85c7e8fdb422
clients:
  - 2187435c-e949-455e-a764-31c82e818f53
  - d91ec932-1f29-402c-bb54-85c7e8fdb422
award_events:
  - 407604a5-99a0-4f5e-997a-ec74c1754fc3
updated_by: 1
updated_at: 1638635109
---
