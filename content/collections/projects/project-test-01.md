---
id: 43a5737d-236e-4966-9944-52eb1a9b347c
blueprint: projects
title: 'project test 01'
thumbnail: test01.jpg
projects_filters:
  - graphic
updated_by: 2
updated_at: 1639136091
filters:
  - graphic
project_filters:
  - graphic
director_filters:
  - david-carrizales
content_bard:
  -
    type: set
    attrs:
      values:
        type: title
        title: Cyborg
  -
    type: set
    attrs:
      values:
        type: text
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut interdum ullamcorper semper. Aenean pulvinar scelerisque tempor. Cras lobortis felis ac orci vulputate elementum. Pellentesque id mauris ornare, dignissim purus ut, finibus elit. In orci orci, dignissim ut felis rutrum, mollis luctus nulla. Proin sed rhoncus nisi. Quisque facilisis velit nisl, at laoreet mauris efficitur ut. Nunc dapibus sodales lorem, at venenatis neque porta a. Praesent tincidunt laoreet vo'
        width: 100%
        position: Left
  -
    type: set
    attrs:
      values:
        type: image
        image: test02.jpg
  -
    type: set
    attrs:
      values:
        type: gallery
        gallery:
          - test03.jpg
          - avatar.png
          - test01.jpg
  -
    type: paragraph
credits:
  - 43f03ba0-8ac6-4b5d-a16e-19d0b6e655d0
  - c5e7f56b-6e74-4859-91e8-0dec5a31b8f7
  - 3c2bf21e-0d02-4d7e-b430-f1794b515540
clients:
  - d91ec932-1f29-402c-bb54-85c7e8fdb422
award_events:
  - 407604a5-99a0-4f5e-997a-ec74c1754fc3
---
