---
id: a2769953-a952-4d42-a0c2-ea374b59b10f
blueprint: projects
title: 'project test 02'
content_bard:
  -
    type: set
    attrs:
      values:
        type: title
        title: 'Title Bard - Test02'
  -
    type: paragraph
seo_description: 'Test 02 Seo'
seo_date: '2021-12-04'
seo_open_graph_image: test02.jpg
credits:
  - c5e7f56b-6e74-4859-91e8-0dec5a31b8f7
  - 3c2bf21e-0d02-4d7e-b430-f1794b515540
clients:
  - 2187435c-e949-455e-a764-31c82e818f53
  - d91ec932-1f29-402c-bb54-85c7e8fdb422
award_events:
  - 407604a5-99a0-4f5e-997a-ec74c1754fc3
mentions:
  - 6a105b5b-12b7-489a-873e-a39f7e027d7c
publications:
  - 8a95350c-910b-4bd9-ab7b-70a2d87b8fc1
updated_by: 2
updated_at: 1639126514
thumbnail: test02.jpg
filters:
  - live-action
---
