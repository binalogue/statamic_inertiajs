---
id: f2b5a763-ba33-4aed-9856-984a3c840566
blueprint: news
title: 'News test01'
content_bard:
  -
    type: set
    attrs:
      values:
        type: text_image
        text: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'
        width: 60%
        position: Right
        image: contacts/bl-14.jpg
  -
    type: paragraph
news_filters:
  - deportes
  - politica
seo_description: 'Lorem ipsum'
seo_date: '2021-12-03'
seo_open_graph_image: contacts/bl-1.jpg
clients:
  - 2187435c-e949-455e-a764-31c82e818f53
updated_by: 2
updated_at: 1639126002
thumbnail: avatar.png
---
