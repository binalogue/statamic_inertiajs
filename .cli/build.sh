#!/bin/zsh

source .cli/alias.sh

# Install Composer dependencies.
docker run --rm \
    -u "$(id -u):$(id -g)" \
    -v $(pwd):/opt \
    -w /opt \
    laravelsail/php80-composer:latest \
    composer install --ignore-platform-reqs

# Copy env file.
[ -f .env ] || ( cp .env.sail .env )

# Start Sail in "detached" mode.
sail up -d

# Generate Laravel encryption key.
sail artisan key:generate
