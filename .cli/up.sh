#!/bin/zsh

source .cli/alias.sh

# Start Sail in "detached" mode.
sail up -d

# Install Composer dependencies.
[ -f composer.lock ] && ( sail composer check-platform-reqs )

sail composer install --prefer-dist --no-progress --no-interaction

# Publish any vendor packages assets.

if [ -f artisan ]; then
    # Migrate database.
    sail artisan migrate:fresh --seed
fi
# Install Yarn dependencies.
sail yarn

# Run Laravel Mix.
sail yarn watch
