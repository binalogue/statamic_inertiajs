<?php

/*
|--------------------------------------------------------------------------
| Statamic Control Panel Collections
|--------------------------------------------------------------------------
|
| All the Statamic collections must be parameterized in this configuration document.
| There are two main indexes pages and collections.
|
| The array per collection should be specified like this:
| key -> method of App\Binalogue\Statamic\Entries\Factory class
| value -> field handle collection
|
| If the key is getEntries, the array should be specified like this:
| key -> prefix_xxx in the blueprint
| value -> collection
*/

return [
    'pages' => [
        'homepage' => [
            'get' => ['title', 'seo_description'],
            'getAsset' => ['seo_open_graph_image'],
            'getBard' => ['content_bard'],
            'getBool' => [],
            'getDate' => ['seo_date'],
            'getEntries' => [
                'featured_a' => ['news', 'projects'],
                'featured_b' => ['news', 'projects']
            ],
            'getSlug' => ['slug'],
        ],
        'work' => [
            'get' => ['title', 'seo_description'],
            'getAsset' => ['seo_open_graph_image'],
            'getBard' => [],
            'getBool' => [],
            'getDate' => ['seo_date'],
            'getEntries' => [
                'projects' => 'projects'
            ],
            'getSlug' => ['slug'],
        ],
        'directors' => [
            'get' => ['title', 'seo_description'],
            'getAsset' => ['seo_open_graph_image'],
            'getBard' => [],
            'getBool' => [],
            'getDate' => ['seo_date'],
            'getEntries' => [
                'projects' => 'projects'
            ],
            'getSlug' => ['slug'],
        ],
        'news' => [
            'get' => ['title', 'seo_description'],
            'getAsset' => ['seo_open_graph_image'],
            'getBard' => [],
            'getBool' => [],
            'getDate' => ['seo_date'],
            'getEntries' => [
                'news' => 'news'
            ],
            'getSlug' => ['slug'],
        ],
        'contact' => [
            'get' => ['title', 'seo_description'],
            'getAsset' => ['seo_open_graph_image'],
            'getBard' => ['content_bard'],
            'getBool' => [],
            'getDate' => ['seo_date'],
            'getEntries' => [],
            'getSlug' => ['slug'],
        ],
        'about' => [
            'get' => ['title', 'seo_description'],
            'getAsset' => ['seo_open_graph_image'],
            'getBard' => ['content_bard'],
            'getBool' => [],
            'getDate' => ['seo_date'],
            'getEntries' => [
                'partners' => 'contacts',
                'clients' => 'contacts',
                'award_events' => 'award_events',
                'mentions' => 'mentions',
                'publications' => 'publications',
            ],
            'getSlug' => ['slug'],
        ],
    ],
    'collections' => [
        'award_children' => [
            'get' => ['title', 'award_video'],
            'getAsset' => [],
            'getBard' => [],
            'getBool' => [],
            'getDate' => ['award_date'],
            'getEntries' => [
                'award_link' => 'links',
            ],
            'getSlug' => ['slug'],
        ],
        'award_events' => [
            'get' => ['title', 'content'],
            'getAsset' => [],
            'getBard' => [],
            'getBool' => [],
            'getDate' => [],
            'getEntries' => [
                'award_children' => 'award_children',
            ],
            'getSlug' => ['slug'],
        ],
        'contacts' => [
            'get' => ['title', 'first_name', 'last_name', 'job_title', 'seo_description'],
            'getAsset' => ['thumbnail', 'seo_open_graph_image'],
            'getBard' => [],
            'getBool' => ['partner', 'client'],
            'getDate' => ['seo_date'],
            'getEntries' => [],
            'getSlug' => ['slug'],
        ],
        'credits' => [
            'get' => ['title'],
            'getAsset' => [],
            'getBard' => [],
            'getBool' => [],
            'getDate' => [],
            'getEntries' => [
                'contacts' => 'contacts',
            ],
            'getSlug' => ['slug'],
        ],
        'links' => [
            'get' => ['title', 'link', 'target'],
            'getAsset' => [],
            'getBard' => [],
            'getBool' => [],
            'getDate' => [],
            'getEntries' => [],
            'getSlug' => ['slug'],
        ],
        'mentions' => [
            'get' => ['title', 'content'],
            'getAsset' => [],
            'getBard' => [],
            'getBool' => [],
            'getDate' => [],
            'getEntries' => [
                'link' => 'links',
            ],
            'getSlug' => ['slug'],
        ],
        'news' => [
            'get' => ['title', 'seo_description'],
            'getAsset' => ['seo_open_graph_image', 'thumbnail'],
            'getBard' => ['content_bard'],
            'getBool' => [],
            'getDate' => ['seo_date'],
            'getEntries' => [
                'clients' => 'contacts',
            ],
            'getSlug' => ['slug'],
            'getTaxonomies' => ['filters'],
        ],
        'projects' => [
            'get' => ['title', 'seo_description'],
            'getAsset' => ['seo_open_graph_image', 'thumbnail'],
            'getBard' => ['content_bard'],
            'getBool' => [],
            'getDate' => ['seo_date'],
            'getEntries' => [
                'credits' => 'credits',
                'clients' => 'contacts',
                'award_events' => 'award_events',
                'mentions' => 'mentions',
                'publications' => 'publications',
            ],
            'getSlug' => ['slug'],
            'getTaxonomies' => ['director_filters','project_filters'],
        ],
        'publications' => [
            'get' => ['title', 'content'],
            'getAsset' => [],
            'getBard' => [],
            'getBool' => [],
            'getDate' => [],
            'getEntries' => [
                'link' => 'links',
            ],
            'getSlug' => ['slug'],
        ],
    ],
];
