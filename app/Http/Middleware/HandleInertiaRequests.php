<?php

namespace App\Http\Middleware;

use Inertia\Middleware;
use Illuminate\Http\Request;
use Statamic\Structures\Nav;

class HandleInertiaRequests extends Middleware
{
    /**
     * The root template that is loaded on the first page visit.
     *
     * @var string
     */
    protected $rootView = 'app';

    /**
     * Determine the current asset version.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    public function version(Request $request)
    {
        return parent::version($request);
    }

    /**
     * Define the props that are shared by default.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function share(Request $request)
    {
        $nav = Nav::findByHandle('main_navbar')->trees()->all()['default']->tree();

        return array_merge(parent::share($request), [

            // Synchronously
            'main_navbar' => $nav

            // Lazily
            // 'auth.user' => fn () => $request->user()
            //     ? $request->user()->only('id', 'name', 'email')
            //     : null,
        ]);
    }
}
