<?php

namespace App\Http\Controllers\Traits;

use Statamic\Entries\Entry;
use App\Binalogue\Statamic\Entries\Factory;

/*
|--------------------------------------------------------------------------
| Statamic Control Panel Collections
|--------------------------------------------------------------------------
|
| The getCollections function of this trait recursively returns all handles fields per collection.
| All the Statamic collections must be parameterized in the config file: config/collections.php
|
*/

trait HasCollections
{
    public function getContent(Entry $entry, string $collection, $level = 1): array
    {
        $content = array();

        if (! is_null($entry))
        {
            $pageFactory = new Factory($entry);

            // array: config/collections.php
            $content = $this->getCollections($pageFactory, $collection, $level);
        }

        return $content;
    }

    public function getCollections(Factory $factory, string $collection = null, $level = 1): array
    {
        static $counter = 0;
        $content = array();

        if (empty($collection))
        {
            $collection = "collections.{$factory->collection()}";
        }

        $content['collection'] = $factory->collection();

        foreach (config("collections.{$collection}") as $method => $handles)
        {
            if (! empty($handles))
            {
                // collection, get, getAsset, getBard, getBool, getDate, getSlug, getTaxonomies
                if (in_array($method, ['collection', 'get', 'getAsset', 'getBard', 'getBool', 'getDate', 'getSlug', 'getTaxonomies']))
                {
                    foreach ($handles as $handle)
                    {
                        $content[$handle] = $factory->$method($handle);
                    }
                }
                // getEntries
                else
                {
                    if ($counter < $level || $level == '*')
                    {
                        ++$counter;

                        foreach ($handles as $handle => $collection)
                        {
                            if (! empty($collection))
                            {
                                $content[$handle] = $factory->getEntries($handle, function(Factory $factory) use ($collection, $level)
                                {
                                    if (! is_array($collection))
                                    {
                                        $output = $this->getCollections($factory, "collections.{$collection}", $level);
                                    }
                                    else
                                    {
                                        foreach ($collection as $value)
                                        {
                                            if ($factory->collection() == $value)
                                            {
                                                $output = $this->getCollections($factory, "collections.{$value}", $level);
                                            }
                                        }
                                    }

                                    return $output;
                                });
                            }
                        }
                    }
                }
            }
        }

        return $content;
    }
}
