<?php

namespace App\Http\Controllers\Pages;

use Inertia\Inertia;
use Inertia\Response;
use Illuminate\Http\Request;
use Statamic\Facades\Collection;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\HasCollections;

/*
|--------------------------------------------------------------------------
| Statamic Control Panel Collections
|--------------------------------------------------------------------------
|
| All the Statamic collections must be parameterized in the config file: config/collections.php
|
*/

class HomepageController extends Controller
{
    use HasCollections;

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request): Response
    {
        $entry = Collection::findByHandle('pages')
            ->queryEntries()
            ->where('slug', 'homepage')
            ->first();

        $content = $this->getContent($entry, 'pages.homepage', 1);

        // resources/js/pages/Homepage.vue
        return Inertia::render('Homepage', [
            'content' => $content
        ]);
    }
}
