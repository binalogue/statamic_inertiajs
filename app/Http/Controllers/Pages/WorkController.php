<?php

namespace App\Http\Controllers\Pages;

use Inertia\Inertia;
use Inertia\Response;
use Statamic\Entries\Entry;
use Illuminate\Http\Request;
use Statamic\Facades\Collection;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\HasCollections;

/*
|--------------------------------------------------------------------------
| Statamic Control Panel Collections
|--------------------------------------------------------------------------
|
| All the Statamic collections must be parameterized in the config file: config/collections.php
|
*/

class WorkController extends Controller
{
    use HasCollections;

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request): Response
    {
        $entry = Collection::findByHandle('pages')
            ->queryEntries()
            ->where('slug', 'work')
            ->first();

        $content = $this->getContent($entry, 'pages.work', 1);

        // resources/js/pages/Projects.vue
        return Inertia::render('Work', [
            'content' => $content
        ]);
    }

    /**
     * @param string  $slug
     * @return Inertia\Response
     */
    public function show(string $slug): Response
    {
        // $entry = Entry::find($slug);
        $entry = Entry::query()
            ->where('slug', $slug)
            ->first();

        $content = $this->getContent($entry, 'collections.projects', '*');

        // resources/js/pages/SingleProject.vue
        return Inertia::render('SingleProject', [
            'content' => $content
        ]);
    }
}
