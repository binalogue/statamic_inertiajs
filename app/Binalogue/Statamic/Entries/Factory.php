<?php

namespace App\Binalogue\Statamic\Entries;

use Carbon\Carbon;
use Statamic\Fields\Value;
use Statamic\Entries\Entry;
use Statamic\Taxonomies\LocalizedTerm;

class Factory
{
    use HasAssets,
        HasEntries;

    protected Entry $entry;

    /**
     * @param Entry $entry
     */
    public function __construct(Entry $entry)
    {
        $this->entry = $entry;
    }

    /**
     * @return mixed
     */
    public function collection()
    {
        return $this->entry->collection()->handle();
    }

    /**
     * @param string $field
     * @return mixed
     */
    public function get(string $field)
    {
        return $this->entry->get($field);
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->entry->slug();
    }

    /**
     * @param string $field
     * @return string|null
     */
    public function getDate(string $field): ?string
    {
        $value = $this->get($field);

        return (! empty($value)) ? Carbon::parse($value)->toFormattedDateString() : null;
    }

    /**
     * @param string $field
     * @return boolean
     */
    public function getBool(string $field): bool
    {
        return (bool) $this->get($field);
    }

    /**
     * @param string $field
     * @return array|null
     */
    public function getTaxonomies(string $field): ?array
    {
        $content = $this->entry->augmentedValue($field)?->value();

        return (! empty($content)) ? $content
            ->map(fn(LocalizedTerm $term) => $term->get('title'))
            ->toArray() : null;
    }

    /**
     * @param string $field
     * @return array|null
     */
    public function getBard(string $field): ?array
    {
        $content = $this->entry->augmentedValue($field)?->value();

        if (is_iterable(($content)))
        {
            foreach ($content as $key => &$section)
            {
                if (in_array($section['type'], ['title', 'subtitle', 'text', 'gallery', 'image', 'video', 'text_image']))
                {
                    foreach ($section as $key2 => &$value)
                    {
                        if ($value instanceof Value)
                        {
                            $raw = $value->raw();

                            if (! empty($raw))
                            {
                                if ($key2 === 'gallery' || $key2 === 'image')
                                {
                                    if (is_array($raw))
                                    {
                                        foreach ($raw as $image)
                                        {
                                            $gallery[] = $this->findAsset($image);
                                        }

                                        $value = $gallery;
                                    }
                                    else
                                    {
                                        $value = $this->findAsset($raw);
                                    }
                                }
                                else
                                {
                                    $value = $raw;
                                }
                            }
                            else
                            {
                                unset($content[$key]);
                                break;
                            }
                        }
                    }
                }
            }

            $content = array_values($content);
        }

        return (! empty($content)) ? $content : null;
    }

    /**
     * @param string $field
     * @return string|null
     */
    public function getYouTubeUrl(string $field): ?string
    {
        $value = $this->get($field);

        return (! empty($value)) ? "https://www.youtube.com/embed/{$value}" : null;
    }
}
