<?php

namespace App\Binalogue\Statamic\Entries;

use Statamic\Facades\Entry;

trait HasEntries
{
    /**
     * @param string $field
     * @param callable $callable
     * @return array|null
     */
    public function getEntries(string $field, callable $callable): ?array
    {
        $value = $this->get($field);

        if (is_iterable($value))
        {
            return collect($value)
                ->map(function($id) use($callable)
                {
                    $entry = Entry::find($id);

                    return (! empty($entry)) ? $callable(new Factory($entry)) : null;
                })
                ->reject(fn($id) => empty($id))
                ->toArray();
        }

        $entry = Entry::find($value);

        return (! empty($entry)) ? $callable(new Factory($entry)) : null;
    }
}
