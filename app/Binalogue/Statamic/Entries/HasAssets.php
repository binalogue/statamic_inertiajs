<?php

namespace App\Binalogue\Statamic\Entries;

use Statamic\Assets\Asset;

trait HasAssets
{
    /**
     * @param string $field
     * @param string $defaultImagePath
     * @return array|null
     */
    public function getAsset(string $field, string $defaultImagePath = null): ?array
    {
        $value = $this->entry->augmentedValue($field)->value();

        if (! (empty($value) || is_null($value)))
        {
            if ($value instanceof Asset)
            {
                return $this->getAssetProperties($value);
            }

            return $value
                ->map(fn(Asset $asset) => $this->getAssetProperties($asset))
                ->toArray();
        }

        return (! empty($defaultImagePath)) ? $this->findAsset($defaultImagePath) : null;
    }

    /**
     * @param string $field
     * @return array
     */
    public function findAsset(string $field): ?array
    {
        $asset = Asset::query()
            ->where('container', 'assets')
            ->where('path', $field)
            ->first();

        if (! $asset instanceof Asset)
        {
            return null;
        }

        return $this->getAssetProperties($asset);
    }

    /**
     * @param Asset $asset
     * @return array
     */
    public function getAssetProperties(Asset $asset): array
    {
        $alt = $asset->get('alt') ?? env('APP_NAME');

        return [
            'url' => $asset->url(),
            'alt' => $alt,
        ];
    }
}
