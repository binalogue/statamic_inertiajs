<?php

namespace Support\Statamic\Listeners;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;
use Statamic\Facades\Term as TermRepository;
use Statamic\Taxonomies\LocalizedTerm;

class FlushCacheListener
{
    /** Create the event listener. */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param object $event
     */
    public function handle($event): void
    {
        $collection = $event->entry->collection()->handle();

        if (in_array($collection, ['companies', 'films', 'institutions'])) {
            Cache::forget($collection);

            $singular = Str::of($collection)->singular();

            TermRepository::query()
                ->where('taxonomy', "{$singular}_categories")
                ->get()
                ->each(function (LocalizedTerm $localizedTerm) use ($collection) {
                    $category = $localizedTerm->slug();
                    Cache::forget("{$collection}.{$category}");
                });
        }

        if ($collection === 'films') {
            Cache::forget('screening-rooms-films');
        }
    }
}
