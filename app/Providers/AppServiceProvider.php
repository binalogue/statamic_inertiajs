<?php

namespace App\Providers;

use Statamic\Statamic;
use Statamic\Facades\CP\Nav;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Statamic::script('app', 'cp');
        // Statamic::style('app', 'cp');

        Nav::extend(function ($nav) {
            $nav->content('Pages')
                ->section('Content')
                ->url('/cp/collections/pages')
                ->icon('pages');
        });
    }
}
