/* Layouts */
import App from "@/Layouts/App";
import MainLayout from "@/Layouts/MainLayout";

export const mainLayout = (h, page) => h(App, [h(MainLayout, [page])]);

export default {
  mainLayout,
};
