<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        \App\Models\User::create([
            'name' => 'Alvaro',
            'email_verified_at' => now(),
            'email' => 'a_rsc@hotmail.com',
            'password' => bcrypt('password'),
            'super' => 1
        ]);
        \App\Models\User::create([
            'name' => 'Cristina',
            'email_verified_at' => now(),
            'email' => 'cristina@binalogue.com',
            'password' => bcrypt('password'),
            'super' => 1
        ]);
        \App\Models\User::create([
            'name' => 'Marcus',
            'email_verified_at' => now(),
            'email' => 'marcus@binalogue.com',
            'password' => bcrypt('password'),
            'super' => 1
        ]);
    }
}
