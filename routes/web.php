<?php

use Inertia\Inertia;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Application;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Main Pages
Route::get('/', HomepageController::class)->name('homepage'); // singular
Route::get('/work', [WorkController::class, 'index'])->name('work.index'); // singular
Route::get('/directors', DirectorsController::class)->name('directors.index'); // plural
Route::get('/news', [NewsController::class, 'index'])->name('news.index'); // plural
Route::get('/about', AboutController::class)->name('about'); // singular
Route::get('/contact', ContactController::class)->name('contact'); // singular

// Other Pages
Route::get('/work/{slug}', [WorkController::class, 'show'])->name('work.show'); // proyecto individual
Route::get('/news/{slug}', [NewsController::class, 'show'])->name('news.show'); // news individual

// Collection Pages
// Route::get('/clients', ClientsController::class)->name('pages.clients'); // plural

Route::get('/test', function () {
    return Inertia::render('Welcome', [
        'test' => 'Binalogue'
    ]);
});

Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

require __DIR__.'/auth.php';
